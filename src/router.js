import Vue from 'vue'
import Router from 'vue-router'
import Propuesta from './views/DetallePropuesta.vue'
import Propuestas from './views/ListadoDePropuestas.vue'
import Bienvenidos from './views/Bienvenidos.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'bienvenidos',
      component: Bienvenidos
    },
    {
      path: '/propuesta',
      name: 'propuesta',
      component: Propuesta
    },
    {
      path: '/propuestas',
      name: 'propuestas',
      component: Propuestas
    }
  ]
})
