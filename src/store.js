import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    temasPropuestos: [],
    modalNuevaPropuesta: false
  },

  getters: {
    temasOrdenados: (state) => {
      return state.temasPropuestos.sort((temaA, temaB) => temaA.puntos > temaB.puntos)
    },
    temasFiltrados: (state, terminoABuscar) => {
      return state.temasPropuestos.filter((tema) => tema.titulo.contains(terminoABuscar))
    }
  },

  mutations: {
    ALTERNAR_MODAL: state => {
      state.modalNuevaPropuesta = !state.modalNuevaPropuesta
    },
    AGREGAR_NUEVO_TEMA: (state, nuevoTema) => {
      state.temasPropuestos.push(nuevoTema)
    }
  },

  actions: {
    alternarModal: (context) => {
      context.commit('ALTERNAR_MODAL')
    },
    proponerTema: (context, temaPropuesto) => {
      context.commit('AGREGAR_NUEVO_TEMA', temaPropuesto)
    }
  }
})
